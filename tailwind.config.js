/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/modules/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  media: false,
  plugins: [require('@tailwindcss/line-clamp')],
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: 'var(--primary-color)',
          hover: 'var(--primary-color-hover)',
        },
        secondary: 'var(--secondary-color)',
        gray: 'var(--gray-color)',
        gray1: '#CCCCCC',
        gray2: '#D3D3D3',
        gray3: '#FCFCFC',
        gray4: '#feebeb',
        red1: '#FF0000',
      },
      animation: {
        fade: 'fadeOut 5s ease-in-out',
      },
      keyframes: (theme) => ({
        fadeOut: {
          '0%': { backgroundColor: theme('colors.red.300') },
          '100%': { backgroundColor: theme('colors.transparent') },
        },
      }),
    },
    screens: {
      xxl: { min: '1600px' },
      // => @media (min-width: 1600px) { ... }

      xl: { max: '1280px' },
      // => @media (max-width: 1279px) { ... }

      lg: { max: '1023px' },
      // => @media (max-width: 1023px) { ... }

      md: { max: '767px' },
      // => @media (max-width: 767px) { ... }

      sm: { max: '639px' },
      // => @media (max-width: 639px) { ... }
    },
  },
  corePlugins: {
    preflight: false,
  },
  mode: 'jit',
};
