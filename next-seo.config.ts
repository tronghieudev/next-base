import { DefaultSeoProps } from 'next-seo';

export const defaultSEO: DefaultSeoProps = {
  defaultTitle: '白鵬引退宮城野襲名披露大相撲｜宮城野部屋公式サイト',
  title: '白鵬引退宮城野襲名披露大相撲｜宮城野部屋公式サイト',
};
