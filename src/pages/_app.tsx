import 'reflect-metadata';
import 'styles/antd/antd.less';
import 'styles/index.css';
import '../../i18n';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { SEO } from 'components/seo';
import { AxiosProvider } from 'containers/axiosProvider';
import { PersistGate } from 'containers/persistGate';
import { RootContainer } from 'containers/root';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { Router } from 'next/router';
// import { appWithTranslation } from 'next-i18next';
import nProgress from 'nprogress';
import { Suspense } from 'react';
import { Provider } from 'react-redux';
import { persistor, storeGlobal } from 'stores';

import { defaultSEO } from '../../next-seo.config';

Router.events.on('routeChangeStart', nProgress.start);
Router.events.on('routeChangeError', nProgress.done);
Router.events.on('routeChangeComplete', nProgress.done);

const queryClient = new QueryClient();

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <SEO configs={defaultSEO} />
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="description" content="description" />
        <meta property="og:url" content="https://url.url" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="title" />
        <meta property="og:description" content="og:description" />
        <meta property="og:site_name" content="og:site_name" />
        <meta property="og:image" content="og:image" />
      </Head>
      <Provider store={storeGlobal}>
        <AxiosProvider>
          <PersistGate persistor={persistor}>
            <QueryClientProvider client={queryClient}>
              <Suspense fallback="loading">
                <RootContainer>
                  <Component {...pageProps} />
                </RootContainer>
              </Suspense>
            </QueryClientProvider>
          </PersistGate>
        </AxiosProvider>
      </Provider>
    </>
  );
}

export default App;
