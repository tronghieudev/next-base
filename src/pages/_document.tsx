import Document, { DocumentContext, Head, Html, Main, NextScript } from 'next/document';
import Script from 'next/script';

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);

    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <Script strategy="afterInteractive" src="https://www.googletagmanager.com/gtag/js?id=G-GA_MEASUREMENT_ID" />
          <Script id="google-analytics" strategy="afterInteractive">
            {`
              window.dataLayer = window.dataLayer || [];
              function gtag(){window.dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'GA_MEASUREMENT_ID');
            `}
          </Script>
          <link rel="manifest" href="/assets/site.webmanifest" />
          <link rel="icon" type="image/x-icon" href="/assets/favicon.ico" />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script src="/script/font-dev.js" async type="text/javascript" />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
