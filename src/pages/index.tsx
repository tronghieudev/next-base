import { Example } from 'modules/example';
import { useTranslation } from 'next-i18next';

const Index = () => {
  const { t } = useTranslation(['common']);

  console.log(t('content'));

  return (
    <>
      <Example />
    </>
  );
};
Index.layout = 'any';

export default Index;
