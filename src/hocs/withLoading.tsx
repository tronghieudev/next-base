import React from 'react';

interface Props {
  loading: boolean;
  children?: React.ReactNode;
}

export function withLoading<T extends Props = Props>(Component: React.ElementType): React.ElementType {
  const HOC = ({ loading, ...props }: T) => {
    return (
      <div>
        {loading && (
          <div className="fixed top-0 left-0 right-0 bottom-0 z-50 flex justify-center items-center" style={{ background: '#ffffffe0' }}>
            <span className="page-loader" />
          </div>
        )}
        <Component {...props} />
      </div>
    );
  };

  return HOC;
}
