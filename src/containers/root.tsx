import { ModalNotification } from 'components/notification/modalNotification';
import { ToastNotification } from 'components/notification/toastNotification';
import { useNotification } from 'hooks/useNotification';
import React from 'react';

import { LayoutWrapper } from '../containers/layoutWrapper';

export const RootContainer: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const { notification } = useNotification();

  return (
    <>
      <LayoutWrapper>{children}</LayoutWrapper>
      <ModalNotification notification={notification} />
      <ToastNotification notification={notification} />
    </>
  );
};
