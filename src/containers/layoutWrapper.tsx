import { Layout } from 'components/layout/index';
import { LAYOUT_TYPE } from 'constants/app';
import router from 'next/router';
import React, { useEffect, useState } from 'react';

export function LayoutWrapper({ children }: { children: React.ReactNode }) {
  const layoutTypeString = (children as any).type?.layout;
  const [isAllowedToVisit, setIsAllowedToVisit] = useState(false);

  useEffect(() => {
    if (!layoutTypeString) {
      setIsAllowedToVisit(true);

      return;
    }

    const isAuth = window.localStorage.getItem('authenticated');

    if (isAuth != '1') {
      router.replace('/jp/basic-authentication');
      setIsAllowedToVisit(false);

      return;
    }

    setIsAllowedToVisit(true);
  }, []);

  if (!isAllowedToVisit) {
    return null;
  }

  if (layoutTypeString === LAYOUT_TYPE.general) {
    return <Layout>{children}</Layout>;
  }

  return <>{children}</>;
}
