import { Pagination } from 'antd';
import React from 'react';

export const App: React.FC = () => <Pagination defaultCurrent={6} total={500} />;
