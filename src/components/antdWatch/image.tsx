import { Image } from 'antd';
import React from 'react';

// eslint-disable-next-line jsx-a11y/alt-text
export const App: React.FC = () => <Image width={200} src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png" />;
