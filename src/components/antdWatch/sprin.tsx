import { Spin } from 'antd';
import React from 'react';

export const App: React.FC = () => (
  <div className="flex justify-center">
    <Spin />
  </div>
);
