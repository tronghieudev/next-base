import { Modal } from 'antd';
import { notificationModalTemplate, notificationType } from 'constants/notification';
import { useTranslation } from 'next-i18next';
import React, { useEffect, useMemo, useState } from 'react';
import { Notification } from 'stores/reducers/app';

interface ModalCommonProps {
  message?: string;
  btnOk?: string;
  btnCancel?: string;
  isOpen?: boolean;
  handleCancel: () => void;
}

interface ModalNotificationProps {
  notification?: Notification | null;
}

export const ModalNotification = ({ notification }: ModalNotificationProps) => {
  const { t } = useTranslation('common');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalType, setModalType] = useState(0);

  const modalCommon = ({ message, btnOk, btnCancel, isOpen, handleCancel }: ModalCommonProps) => {
    return (
      <Modal title="Basic Modal" open={isOpen} onCancel={handleCancel}>
        <p>{message || 'This is message default'}</p>
      </Modal>
    );
  };

  useEffect(() => {
    if (notification?.type && notification?.type === notificationType.MODAL) {
      setIsModalOpen(true);

      notification?.template && setModalType(notification?.template);
    }
  }, [notification]);

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  let modal;
  switch (modalType) {
    case notificationModalTemplate.TEMPLATE_1:
      modal = modalCommon({
        isOpen: isModalOpen,
        handleCancel: handleCancel,
        message: 'This is message of modal template 1',
      });
      break;

    case notificationModalTemplate.TEMPLATE_2:
      modal = modalCommon({
        isOpen: isModalOpen,
        handleCancel: handleCancel,
        message: 'This is message of modal template 2',
      });
      break;

    default:
      modal = modalCommon({
        isOpen: isModalOpen,
        handleCancel: handleCancel,
      });
      break;
  }

  return modal;
};
