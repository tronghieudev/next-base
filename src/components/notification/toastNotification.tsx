import { notification as antdNotification } from 'antd';
import { notificationToastTemplate, notificationType } from 'constants/notification';
import { useTranslation } from 'next-i18next';
import { useEffect } from 'react';
import { Notification } from 'stores/reducers/app';

interface ToastCommonProps {
  message?: string;
  description?: string;
  handleCancel?: () => void;
}

interface ToastNotificationProps {
  notification?: Notification | null;
}

export const ToastNotification = ({ notification }: ToastNotificationProps) => {
  const { t } = useTranslation('common');

  useEffect(() => {
    if (notification?.type && notification?.type === notificationType.TOAST) {
      switch (notification?.template) {
        case notificationToastTemplate.TEMPLATE_1:
          toastCommon({
            handleCancel: handleCancel,
            message: 'This is message of toast template 1',
          });
          toastCommon({});
          break;

        case notificationToastTemplate.TEMPLATE_2:
          toastCommon({
            handleCancel: handleCancel,
            message: 'This is message of toast template 2',
          });
          break;

        default:
          toastCommon({
            handleCancel: handleCancel,
          });
          break;
      }
    }
  }, [notification]);

  const toastCommon = ({ message, description, handleCancel }: ToastCommonProps) => {
    return antdNotification.open({
      message: message || 'This is notification title default',
      description: description || 'This is description default',
      duration: 4,
    });
  };

  const handleCancel = () => null;

  return null;
};
