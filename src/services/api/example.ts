import { APIConfig } from 'types/api';

export const createExampleAPI: APIConfig = {
  endPoint: '/success',
  keys: ['example'],
  method: 'POST',
};

export const updateExampleAPI: APIConfig = {
  endPoint: '/error?status=400',
  keys: ['example'],
  method: 'GET',
};
