import { GetStaticProps, GetStaticPropsContext } from 'next';
import { i18n } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import i18nextConfig from '../../next-i18next.config';

export const getI18nPaths = () =>
  i18nextConfig.i18n.locales.map((lng) => ({
    params: {
      locale: lng,
    },
  }));

export const getStaticPaths = () => ({
  fallback: false,
  paths: getI18nPaths(),
});

export const getI18nProps = async (ctx: GetStaticPropsContext, ns = ['common']) => {
  const locale = ctx?.params?.locale as string;
  if (process.env.NODE_ENV === 'development') {
    await i18n?.reloadResources();
  }

  const props = {
    ...(await serverSideTranslations(locale, ns)),
  };

  return props;
};

export const makeStaticProps =
  (ns: string[] = []): GetStaticProps =>
  async (ctx) => ({
    props: await getI18nProps(ctx, ns),
  });
