import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface Metadata {
  [index: string]: string | number;
}

export interface Error {
  code?: number;
  message?: string;
  metadata?: Metadata;
}

export interface Notification {
  type?: 'modal' | 'toast';
  template?: number;
  metadata?: Metadata;
  errors?: Error[] | null;
}

export interface IAppState {
  authLoaded: boolean;
  notification: Notification | null;
}

const initialState: IAppState = {
  authLoaded: false,
  notification: null,
};

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setAuthenticationLoaded: (state, action: PayloadAction<boolean>) => {
      state.authLoaded = action.payload;
    },
    setNotification: (state, action: PayloadAction<Notification>) => {
      state.notification = action.payload;
    },
  },
});

export const appReducer = appSlice.reducer;
export const appAction = appSlice.actions;
