export const notificationType = {
  MODAL: 'modal',
  TOAST: 'toast',
};

export const notificationModalTemplate = {
  TEMPLATE_1: 1,
  TEMPLATE_2: 2,
};

export const notificationToastTemplate = {
  TEMPLATE_1: 1,
  TEMPLATE_2: 2,
};
