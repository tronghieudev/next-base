export class PaginationReq {
  limit?: number;
  page?: number;
}
