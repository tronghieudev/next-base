import { assignDataToInstance } from 'utilities/helper';

export class ExampleReqDto {
  email: string;
  name: string;

  constructor(data?: Partial<ExampleReqDto>) {
    assignDataToInstance(data, this);
  }
}
