/* eslint-disable @typescript-eslint/no-explicit-any */
import { Type } from 'class-transformer';
import { Error } from 'stores/reducers/app';

export class ResTemplate<TData> {
  data?: TData;
  success: boolean;
  statusCode?: number;

  errors?: Error[];
}

export class PaginationRes<TData> {
  pagination: {
    page: number;
    perPage: number;
    total: number;
  };
  items?: TData[];
}

export const convertDtoToTemplateDto = <TData>(dto: any) => {
  class ResponseTemplateData extends ResTemplate<TData> {
    @Type(() => dto)
    data?: TData;
  }

  return ResponseTemplateData;
};
