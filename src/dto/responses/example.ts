import { Expose, Transform } from "class-transformer";

export class ExampleResDto {
  @Expose()
  @Transform(({obj}) => obj?._key)
  key: string;

  email: string;
  name: string;
}
