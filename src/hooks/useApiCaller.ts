/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  useMutation as useMutationRoot,
  UseMutationOptions,
  UseMutationResult as UseMutationResultRoot,
  useQuery as useQueryRoot,
  UseQueryOptions,
  UseQueryResult,
} from '@tanstack/react-query';
import { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { instanceToPlain, plainToInstance } from 'class-transformer';
import { responseStatusCode } from 'constants/responseStatusCode';
import { useAxios } from 'containers/axiosProvider';
import { convertDtoToTemplateDto, ResTemplate } from 'dto/responses/template';
import { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
import { Error, Notification } from 'stores/reducers/app';
import { APIConfig } from 'types/api';

type FetcherProps<TVariables> = {
  data?: TVariables;
  dataResDto?: any;
  dispatch: Dispatch;
  apiConfig: APIConfig;
  axios: AxiosInstance;
};

interface EndPointParameter {
  [index: string]: string | number;
}

interface MutationOptionProps {
  parameters?: EndPointParameter;
}

interface ResponseStatusProps<TResponseData> {
  notification?: Notification;
  handler?: (result?: ResTemplate<TResponseData>) => any;
  dispatch: Dispatch;
  errors?: Error[];
  dataResponse?: ResTemplate<TResponseData>;
}

type UseMutationResult<TData = unknown, TError = unknown, TVariables = unknown, TContext = unknown> = UseMutationResultRoot<
  TData,
  TError,
  TVariables,
  TContext
> & {
  callMutationAsync: (data: TVariables, mutationOption?: MutationOptionProps) => void;
};

type QueryOption = Omit<UseQueryOptions, 'queryKey' | 'queryFn' | 'initialData'> & {
  initialData?: () => undefined;
};

type ErrorObject<TResponseData> = {
  notification?: Notification;
  handler?: (result?: ResTemplate<TResponseData>) => any;
};

type SuccessObject<TResponseData> = {
  notification?: Notification;
  handler?: (result?: ResTemplate<TResponseData>) => any;
};

type Option<TResponseData> = QueryOption &
  UseMutationOptions & {
    errorObj?: ErrorObject<TResponseData>;
    successObj?: SuccessObject<TResponseData>;
    parameters?: EndPointParameter;
  };

type Props<TVariables, TResponseData> = {
  apiConfig: APIConfig;
  data?: TVariables;
  option?: Option<TResponseData>;
  dataResDto?: any;
};

const handleApiEndpoint = (endPoint: string, parameters?: EndPointParameter): string => {
  if (!parameters) {
    return endPoint;
  }

  const regex = /{(.*?)}/g;
  let newEndPoint = endPoint;
  let match: string[] | null;

  while ((match = regex.exec(endPoint))) {
    if (match === null) {
      continue;
    }

    const keyMatch: string = match?.[0];
    const parameterMatch: string = match?.[1];

    if (!keyMatch || !parameterMatch) {
      continue;
    }

    const parameter = parameters?.[parameterMatch];

    newEndPoint = newEndPoint.replace(keyMatch, parameter ? parameter.toString() : keyMatch);
  }

  return newEndPoint;
};

function handleFetcherResponseStatus<TResponseData>(dataResponse: ResTemplate<TResponseData>, dispatch: Dispatch, option?: Option<TResponseData>) {
  if (dataResponse.success === true) {
    handleResponseStatus({
      notification: option?.successObj?.notification,
      handler: option?.successObj?.handler,
      dispatch: dispatch,
      errors: [],
      dataResponse: dataResponse,
    });

    return;
  }

  return handleResponseStatus({
    notification: option?.errorObj?.notification,
    handler: option?.errorObj?.handler,
    dispatch: dispatch,
    errors: dataResponse?.errors || [],
    dataResponse: dataResponse,
  });
}

function handleResponseStatus<TResponseData>({ notification, handler, dispatch, errors, dataResponse }: ResponseStatusProps<TResponseData>) {
  if (handler) {
    handler(dataResponse);

    return;
  }

  const appNotification = notification || { errors: [] };
  appNotification.errors = [...(appNotification?.errors || []), ...(errors || [])];

  dispatch({
    type: 'app/setNotification',
    payload: appNotification,
  });
}

export async function fetcher<TVariables = unknown, TData = unknown>(
  { apiConfig, data, axios, dataResDto, dispatch }: FetcherProps<TVariables>,
  option?: Option<TData>
): Promise<ResTemplate<TData>> {
  // Handle api endpoint
  apiConfig.endPoint = handleApiEndpoint(apiConfig.endPoint, option?.parameters);

  let request: Promise<AxiosResponse<TData>> = null as any;
  let statusCode = 0;

  if (apiConfig?.factoryData) {
    statusCode = responseStatusCode.OK;
    request = new Promise((resolve) => {
      setTimeout(() => resolve(apiConfig?.factoryData), 2000);
    });
  }

  const dataTransform = instanceToPlain(data, {
    exposeDefaultValues: true,
  }) as any;

  if (!request) {
    switch (apiConfig.method) {
      case 'POST':
        request = axios.post<TData, AxiosResponse<TData>>(apiConfig.endPoint, dataTransform, {
          withCredentials: false,
          headers: { 'Access-Control-Allow-Origin': '*' },
        });
        break;

      case 'GET':
        request = axios.get(apiConfig.endPoint, { params: dataTransform });
        break;

      case 'DELETE':
        request = axios.delete(apiConfig.endPoint, { params: dataTransform });
        break;

      case 'PUT':
        request = axios.put(apiConfig.endPoint, dataTransform);
        break;
    }
  }

  let dataResponse: ResTemplate<TData> = { success: false };
  try {
    const result = await request;

    dataResponse =
      plainToInstance(
        convertDtoToTemplateDto<TData>(dataResDto),
        result.data
        // { exposeDefaultValues: true }
      ) || ({} as ResTemplate<TData>);
    dataResponse.success = true;

    if (!statusCode) {
      statusCode = result.status;
    }

    dataResponse.statusCode = statusCode;

    return dataResponse;
  } catch (error) {
    const axiosError = error as AxiosError;

    dataResponse =
      plainToInstance(
        convertDtoToTemplateDto<TData>(dataResDto),
        axiosError.response?.data
        // { exposeDefaultValues: true }
      ) || ({} as ResTemplate<TData>);

    if (typeof dataResponse !== 'object') {
      dataResponse = {} as ResTemplate<TData>;
    }

    dataResponse.success = false;
    dataResponse.statusCode = axiosError.response?.status;

    return dataResponse;
  } finally {
    handleFetcherResponseStatus(dataResponse, dispatch, option);
  }
}

export function useQuery<TVariables, TResponseData>({
  apiConfig,
  data,
  dataResDto,
  option,
}: Props<TVariables, TResponseData>): UseQueryResult<ResTemplate<TResponseData>> {
  const axios = useAxios();
  const dispatch = useDispatch();

  const queryKeys = useMemo(() => {
    let keys: any[] = [];

    if (apiConfig.keys.length) {
      keys = [...keys, ...apiConfig.keys];
    }

    if (data) {
      keys = [...keys, data];
    }

    return keys;
  }, [apiConfig, data]);

  const queryResult = useQueryRoot<ResTemplate<TResponseData>>(
    queryKeys,
    () => fetcher<TVariables, TResponseData>({ apiConfig, data, axios, dataResDto, dispatch }, option),
    {
      refetchOnWindowFocus: false,
      ...(option || ({} as any)),
    }
  );

  return queryResult;
}

export function useMutation<TVariables = unknown, TResponseData = unknown>({
  apiConfig,
  option,
  dataResDto,
}: Props<TVariables, TResponseData>): UseMutationResult<ResTemplate<TResponseData>, unknown, TVariables> {
  let options = option;
  const axios = useAxios();
  const dispatch = useDispatch();

  const mutation = useMutationRoot<ResTemplate<TResponseData>, unknown, TVariables>(
    (data: TVariables) => fetcher<TVariables, TResponseData>({ apiConfig, data, axios, dataResDto, dispatch }, options),
    option as any
  );

  async function callMutationAsync(data: TVariables, mutationOption?: MutationOptionProps) {
    if (mutationOption) {
      options = { ...options, ...mutationOption };
    }

    const result = await mutation.mutateAsync(data);

    return result;
  }

  return { ...mutation, callMutationAsync };
}
