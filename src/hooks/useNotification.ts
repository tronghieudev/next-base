import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'stores';

export const useNotification = () => {
  const notification = useSelector((state) => state.app.notification);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!notification) {
      return;
    }

    dispatch({
      type: 'app/setNotification',
      payload: null,
    });
  }, [notification]);

  return { notification };
};
