import { useEffect, useState } from 'react';

export function useCountdown() {
  const [seconds, setSeconds] = useState<string>('00');
  const [minutes, setMinutes] = useState<string>('00');
  const [hours, setHours] = useState<string>('00');
  const [days, setDays] = useState<string>('0');

  useEffect(() => {
    const countDownDate = new Date('Jan 28, 2023 10:00:00').getTime();
    const countdown = setInterval(function () {
      const now = new Date().getTime();

      const distance = countDownDate - now - 1000 * 60 * 60 * 2;

      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      setSeconds(seconds < 10 ? `0${seconds}` : `${seconds}`);
      setMinutes(minutes < 10 ? `0${minutes}` : `${minutes}`);
      setHours(hours < 10 ? `0${hours}` : `${hours}`);
      setDays(String(days));
    }, 1000);

    return () => {
      clearInterval(countdown);
    };
  }, []);

  return { seconds, minutes, hours, days };
}
