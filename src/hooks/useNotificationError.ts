import { useEffect, useState } from 'react';
import { useSelector } from 'stores';
import { Error } from 'stores/reducers/app';

export const useNotificationError = () => {
  const notification = useSelector((state) => state.app.notification);
  const [errors, setErrors] = useState<Error[] | null>(null);

  useEffect(() => {
    if (notification?.errors) {
      setErrors(notification?.errors);
    }
  }, [notification]);

  const setErrorsEmpty = () => {
    setErrors(null);
  };

  return { errors, setErrorsEmpty };
};
