import { Card, Typography } from 'antd';

const { Title } = Typography;
interface CardProps {
  title?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  children?: any;
}
export const CardComponent = ({ children, title }: CardProps) => {
  return (
    <Card title={<Title level={3}>{title}</Title>} className="mt-5">
      {children}
    </Card>
  );
};
