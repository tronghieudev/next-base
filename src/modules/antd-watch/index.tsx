import { App as Alert } from 'components/antdWatch/alert';
import { App as AutoComplete } from 'components/antdWatch/autoComplete';
import { App as Avatar } from 'components/antdWatch/avatar';
import { App as Badge } from 'components/antdWatch/badge';
import { App as Button } from 'components/antdWatch/button';
import { App as Calendar } from 'components/antdWatch/calendar';
import { App as Card } from 'components/antdWatch/card';
import { App as Carousel } from 'components/antdWatch/carousel';
import { App as Cascader } from 'components/antdWatch/cascader';
import { App as CheckBox } from 'components/antdWatch/checkbox';
import { App as Collapse } from 'components/antdWatch/collapse';
import { App as Comment } from 'components/antdWatch/comment';
import { App as DatePicker } from 'components/antdWatch/datePicker';
import { App as Drawer } from 'components/antdWatch/drawer';
import { App as DropDown } from 'components/antdWatch/dropDown';
import { App as Form } from 'components/antdWatch/form';
import { App as Image } from 'components/antdWatch/image';
import { App as Input } from 'components/antdWatch/input';
import { App as List } from 'components/antdWatch/list';
import { App as Menu } from 'components/antdWatch/menu';
import { App as Message } from 'components/antdWatch/message';
import { App as Modal } from 'components/antdWatch/modal';
import { App as Notification } from 'components/antdWatch/notification';
import { App as Pagination } from 'components/antdWatch/pagination';
import { App as PopConfirm } from 'components/antdWatch/popConfirm';
import { App as PopOver } from 'components/antdWatch/popOver';
import { App as Progress } from 'components/antdWatch/progress';
import { App as Radio } from 'components/antdWatch/radio';
import { App as Rate } from 'components/antdWatch/rate';
import { App as Result } from 'components/antdWatch/result';
import { App as Segmented } from 'components/antdWatch/segmented';
import { App as Skeleton } from 'components/antdWatch/skeleton';
import { App as Slider } from 'components/antdWatch/slider';
import { App as Sprin } from 'components/antdWatch/sprin';
import { App as Statistic } from 'components/antdWatch/statistic';
import { App as Steps } from 'components/antdWatch/steps';
import { App as Switch } from 'components/antdWatch/switch';
import { App as Table } from 'components/antdWatch/table';
import { App as Tabs } from 'components/antdWatch/tabs';
import { App as Tag } from 'components/antdWatch/tag';
import { App as TimeLine } from 'components/antdWatch/timeLine';
import { App as ToolTip } from 'components/antdWatch/toolTip';
import { App as Transfer } from 'components/antdWatch/transfer';
import { App as TreeSelect } from 'components/antdWatch/treeSelect';
import { App as Typography } from 'components/antdWatch/typography';
import { App as Upload } from 'components/antdWatch/upload';
import React from 'react';

import { CardComponent } from './componentCard';

export const Index = () => {
  const components = [
    { component: Alert, name: 'Alert' },
    { component: AutoComplete, name: 'AutoComplete' },
    { component: Avatar, name: 'Avatar' },
    { component: Badge, name: 'Badge' },
    { component: Button, name: 'Button' },
    { component: Calendar, name: 'Calendar' },
    { component: Card, name: 'Card' },
    { component: Carousel, name: 'Carousel' },
    { component: Cascader, name: 'Cascader' },
    { component: CheckBox, name: 'CheckBox' },
    { component: Collapse, name: 'Collapse' },
    { component: Comment, name: 'Comment' },
    { component: DatePicker, name: 'DatePicker' },
    { component: Drawer, name: 'Drawer' },
    { component: DropDown, name: 'DropDown' },
    { component: Form, name: 'Form' },
    { component: Image, name: 'Image' },
    { component: Input, name: 'Input' },
    { component: List, name: 'List' },
    { component: Menu, name: 'Menu' },
    { component: Message, name: 'Message' },
    { component: Modal, name: 'Modal' },
    { component: Notification, name: 'Notification' },
    { component: Pagination, name: 'Pagination' },
    { component: PopConfirm, name: 'PopConfirm' },
    { component: PopOver, name: 'PopOver' },
    { component: Progress, name: 'Progress' },
    { component: Radio, name: 'Radio' },
    { component: Rate, name: 'Rate' },
    { component: Result, name: 'Result' },
    { component: Segmented, name: 'Segmented' },
    { component: Skeleton, name: 'Skeleton' },
    { component: Slider, name: 'Slider' },
    { component: Sprin, name: 'Sprin' },
    { component: Statistic, name: 'Statistic' },
    { component: Steps, name: 'Steps' },
    { component: Switch, name: 'Switch' },
    { component: Table, name: 'Table' },
    { component: Tabs, name: 'Tabs' },
    { component: Tag, name: 'Tag' },
    { component: TimeLine, name: 'TimeLine' },
    { component: ToolTip, name: 'ToolTip' },
    { component: Transfer, name: 'Transfer' },
    { component: TreeSelect, name: 'TreeSelect' },
    { component: Typography, name: 'Typography' },
    { component: Upload, name: 'Upload' },
  ];

  return (
    <div className="py-5 px-10 bg-gray-200">
      {components.map((Item, index) => (
        <CardComponent title={Item.name} key={index}>
          <Item.component />
        </CardComponent>
      ))}
    </div>
  );
};
