import { ExampleReqDto } from 'dto/requests';
import { useNotificationError } from 'hooks/useNotificationError';
import { useTranslation } from 'next-i18next';
import { useEffect } from 'react';

import { useLogic } from './useLogic';

export const Example = () => {
  const { t } = useTranslation();

  const { updateExample } = useLogic();
  const { errors, setErrorsEmpty } = useNotificationError();

  useEffect(() => {
    updateExample(
      new ExampleReqDto({
        email: 'email@email.vn',
        name: 'Hieu',
      })
    );
  }, []);

  return (
    <>
      <h1>{t('translation')}</h1>
    </>
  );
};
