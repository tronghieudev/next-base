import { ExampleReqDto } from 'dto/requests';
import { ExampleResDto } from 'dto/responses';
import { useMutation, useRouterState } from 'hooks';
import { updateExampleAPI } from 'services/api';

export const useLogic = () => {
  const [input, setInput, isReady] = useRouterState<ExampleReqDto>({
    initialValue: new ExampleReqDto(),
  });

  const updateExampleMutation = useMutation<ExampleReqDto, ExampleResDto>({
    apiConfig: updateExampleAPI,
    option: {
      enabled: true,
      errorObj: {
        // notification: { type: 'toast', template: notificationModalTemplate.TEMPLATE_2 },
      },
    },

    dataResDto: ExampleReqDto,
  });

  const updateExample = async (data: ExampleReqDto) => {
    const result = updateExampleMutation.callMutationAsync(data, {
      parameters: { id: 1 },
    });

    return result;
  };

  return { updateExample };
};
